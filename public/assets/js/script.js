//window.onload = function()
//{
    //for the canvas
    const canvas = document.getElementById("the_canvas")
    const context = canvas.getContext("2d");

    //variables
    let currentLoopIndex = 0;
    let currentLoopIndex2 = 0;
    let frameCount = 0;
    let frameCount2 = 0;
    let currentDirection = 0;
    let enemyDirection = 0;
    let keyPressed = "default"
    let qPressed = false;
    let sPressed = false;
    let speed = 1;
    let enterPushed = false;
    let enemyMoveUp = false;
    let spaceBar = 0;
    let character = 0;
    let fillVal = 0;
    
    //let scoreCount = 0;
    let timeEnd = false;
    const scale = 2;
    const width = 32; 
    const height = 32;
    const scaledWidth = scale*width;
    const scaledHeight = scale*height;
    const walkLoop = [0, 1, 2, 0];
    const frameLimit = 7;
    const secondImages = 3;
    const startImage2 = width * secondImages;
    const thirdImageX = 3;
    const thirdImageY = 4;
    const startImage3X = width * thirdImageX;
    const startImage3Y = height * thirdImageY;

    //images
    let image = new Image();
    image.src = "assets/img/wizard.png";
    let gameOverImg = new Image();
    gameOverImg = "./assets/img/game-over.jpg";
    let ball = new Image();
    ball = "assets/img/red-pokeball.png";

    let player = new GameObject(image, 0, 20, 100, 100);
    let enemy = new GameObject(image, 1000, 20, 100, 100);
    let endScreen = new GameObject(gameOverImg, 0, 0, 1100, 500);
    let pokeball = new GameObject(ball, 500, 250, 32, 32);

    const name = localStorage.getItem('username');

    function addName(){
        let header = document.getElementById("main-header");
        header.innerHTML = "Hello " + name;
    }

    addName();

    //function to animate player
    function animate(){
        if (gamerInput.action != "None" && gamerInput.action != "Spacebar"){
            frameCount++;
            if(frameCount >= frameLimit){
                frameCount = 0;
                currentLoopIndex++;
                if(currentLoopIndex >= walkLoop.length){
                    currentLoopIndex = 0;
                }
            }
        }
        else{
            currentLoopIndex = 0;
        } 
    }

    //function to animate enemy
    function animateEnemy(){
        frameCount2++;
        //console.log(frameCount2);
        if(frameCount2 >= frameLimit){
            frameCount2 = 0;
            currentLoopIndex2++;
            //console.log(currentLoopIndex2);
            if(currentLoopIndex2 >= walkLoop.length){
                currentLoopIndex2 = 0;
            }
        } 
    }

     // GameObject holds positional information
    // Can be used to hold other information based on requirements
    function GameObject(spritesheet, x, y, width, height) {
        this.spritesheet = spritesheet;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.mvmtDirection = "None";
    }  

    // The GamerInput is an Object that holds the Current
    // GamerInput (Left, Right, Up, Down, MouseClicks)
    function GamerInput(input) {
        this.action = input; // Hold the current input as a string
    }

    // Default GamerInput is set to None
    let gamerInput = new GamerInput("None"); //No Input


    function input(event) {
        // Take Input from the Player
        // console.log("Input");
        // console.log("Event type: " + event.type);
        // console.log("Keycode: " + event.keyCode);
        if (event.type === "keydown") {
            switch (event.keyCode) {
                case 37: // Left Arrow
                    gamerInput = new GamerInput("Left");
                    break; //Left key
                case 38: // Up Arrow
                    gamerInput = new GamerInput("Up");
                    break; //Up key
                case 39: // Right Arrow
                    gamerInput = new GamerInput("Right");
                    break; //Right key
                case 40: // Down Arrow
                    gamerInput = new GamerInput("Down");
                    break; //Down key
                case 81: //Q key
                    gamerInput = new GamerInput("qKey");
                    break;
                case 83://S key
                    gamerInput = new GamerInput("sKey");
                    break;
                case 13: //enter key
                    gamerInput = new GamerInput("Enter");
                    break;
                case 32:
                    gamerInput = new GamerInput("Spacebar");
                    changeCharacter();
                    break;
                default:
                    gamerInput = new GamerInput("None"); //No Input
            }
        } else {
            gamerInput = new GamerInput("None");
        }
    }

    //spritesheetc ref 
    //for 1st character
    //row 0 down
    //row 1 left
    //row 2 right
    //row 3 up
    function update() {
        // console.log("Update");
        // Check Input
        if (gamerInput.action === "Up") {
            console.log("Move Up");
            currentDirection = 3;
            keyPressed = "up";
        } else if (gamerInput.action === "Down") {
            console.log("Move Down");
            currentDirection = 0;
            keyPressed = "down";
        } else if (gamerInput.action === "Left") {
            console.log("Move Left");
            currentDirection = 1;
            keyPressed = "left";
        } else if (gamerInput.action === "Right") {
            console.log("Move Right");
            currentDirection = 2;
            keyPressed = "right";
        } else if (gamerInput.action === "qKey"){
            console.log("q key pressed");
            qPressed = "true";
        } else if (gamerInput.action === "sKey"){
            console.log("s key pressed");
            sPressed = "true";
        } else if (gamerInput.action === "Enter") {
            console.log("Enter - start animation");
            enterPushed = true;
        } else if (gamerInput.action === "Spacebar"){
            console.log("spacebar - switch player" + spaceBar)
        }
    }

    //function to move main player around canvas
    //using arrow keys on keyboard
    function movePlayer(){
        if(keyPressed == "up"){
            player.y -= speed; // Move Player Up
            keyPressed = "default";
        }
        else if(keyPressed == "down"){
            player.y += speed; // Move Player Down
            keyPressed = "default";
        }
        else if(keyPressed == "left"){
            player.x -= speed; // Move Player Left
            keyPressed = "default";
        }
        else if(keyPressed == "right"){
            player.x += speed; // Move Player Right
            keyPressed = "default";
        }

        if(qPressed == "true"){
            speed = 4;
            qPressed = "false";
        }

        if(sPressed == "true"){
            speed = 1;
            sPressed = "false";  
        }
    }

    //functions to move player with dpad
    function buttonA(){
        gamerInput = new GamerInput("Right"); 
    }
    function buttonB(){
        gamerInput = new GamerInput("Down");
    }
    function buttonX(){
        gamerInput = new GamerInput("Up"); //move player up
    }
    function buttonY(){
        gamerInput = new GamerInput("Left"); //move player left
    }
    function stopClick(){
        gamerInput = new GamerInput("None");
    }

    //function to move enemy up + down canvas with 1 input
    //direction ref for enemy
    //row 0 = down
    //row 3 = up
    function moveEnemy(){
        if(enemyMoveUp == true){
            enemy.y--;
            enemyDirection = 3;//moving up
        }
        else{
            enemy.y++;
            enemyDirection = 0;//moving down
        }

        if(enemy.y >= 468){
            enemy.y = 467;
            enemyMoveUp = true;
        }
        else if(enemy.y <= 0){
            enemy.y = 1;
            enemyMoveUp = false;
        }
    }

    //function to change
    function changeCharacter(){
        //first character
        if(spaceBar == 0){
            character = 0;
            spaceBar = 1;
        }
        else if(spaceBar == 1){
            character= 1;
            spaceBar = 0;
        }
    }

    //nipple code
    //   let dynamic = nipplejs.create({
    //      color: 'orange',
    //  });

    //  dynamic.on('added', function (evt, nipple){
    //      nipple.on('dir:up', function (evt, data){
    //         //console.log("direction up");
    //         gamerInput = new GamerInput("Up");
    //     });
    //     nipple.on('dir:down', function (evt, data){
    //         //console.log("direction down");
    //         gamerInput = new GamerInput("Down");
    //     });
    //     nipple.on('dir:left', function (evt, data){
    //         //console.log("direction left");
    //         gamerInput = new GamerInput("Left");
    //     });
    //     nipple.on('dir:right', function (evt, data){
    //         //console.log("direction right");
    //         gamerInput = new GamerInput("Right");
    //     });
    //     nipple.on('end', function (evt, data){
    //         //console.log("move stopped");
    //         gamerInput = new GamerInput("None");
    //     });
    // });
    
    
    //drawing a timer bar
    function drawTimer(){
        let width = 1100;
        let height = 20;
        let max = 100;
        let val = 10;
       

        //drawing background
        context.fillStyle = "#000000";
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.fillRect(0, 0, width, height);

        //drawing fill
        context.fillStyle = "#00ff00";
        //let fillVal = Math.min(Math.max(val/max, 0), 1);
        context.fillRect(0, 0, fillVal*width, height);
        fillVal += 0.0003;

        if(fillVal >= 1.0){ //1.0 is a good time
            timeEnd = true;
        }
    }

    //drawing score on canvas
    function writeScore(){
        let scoreString =  "score: "+ scoreCount;
        context.font = '22px sans-serif';
        context.fillText(scoreString, 1000, 480);
    }

    function collision(){
        if(enemy.x < player.x + width && enemy.x + width > player.x 
            && enemy.y < player.y + height && enemy.y + height > player.y){
                console.log("collision");
                scoreCount--;
                localStorage.setItem("score", scoreCount);
                player.x = 0;
                player.y = 20;
            }
    }

    //drawing funtion
    function draw() {
        context.clearRect(0, 0, canvas.width, canvas.height);
       
        drawTimer();
        if(!timeEnd){
            writeScore();
            animate();
            //context.drawImage(pokeball, 500, 250, 32, 32);
            if(enterPushed == true){
                animateEnemy();
            }
            
            context.drawImage(enemy.spritesheet, startImage2+ (width*walkLoop[currentLoopIndex2]), 
                                enemyDirection*height, width, height, enemy.x, enemy.y, width, height);
            
            if(character==0){
                context.drawImage(player.spritesheet, walkLoop[currentLoopIndex]*width, 
                    currentDirection*height, width, height, player.x, 
                    player.y, width, height);
            }
            else if(character == 1){
                context.drawImage(player.spritesheet, startImage3X+(walkLoop[currentLoopIndex]*width), 
                    startImage3Y+(currentDirection*height), width, height, player.x, 
                    player.y, width, height);
            }
            collision();
        }
        else {
            context.drawImage(endScreen, 0, 0, 100, 100);
        }
        //
       
        
    }

    //the game loop - 
    // calling the request animation frame again to make the loop
    function gameloop() {
        update();
        draw();
        movePlayer();
        
        if(enterPushed==true){
            moveEnemy();
        }
 
        window.requestAnimationFrame(gameloop);
    }

    // Handle Active Browser Tag Animation
    window.requestAnimationFrame(gameloop);


    window.addEventListener('keydown', input);
    // disable the second event listener if you want continuous movement
    window.addEventListener('keyup', input);
//}